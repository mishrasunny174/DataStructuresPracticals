#include <iostream>
#include <cstdlib>

using namespace std;

#ifdef __linux__
#define CLRSCR "clear"
#else
#define CLRSCR "cls"
#endif

template <typename T>
int binarySearch(T *array, int end, T data, int start = 0);

template <typename T>
int linearSearch(T *array, int size, T data);

template <typename T>
void display(T *array, int size);

int main()
{
    int size, choice, *array, element;
    cout << "Enter size of array: ";
    cin >> size;
    array = new int[size];
    cout << "Enter elements of the array" << endl;
    for (int i = 0; i < size; i++)
    {
        cout << "Enter element no " << i + 1 << " : ";
        cin >> array[i];
    }
    while (1)
    {
        system(CLRSCR);
        cout << "Array is: ";
        display(array, size);
        cout << "1) perform linear search" << endl;
        cout << "2) perform binary search" << endl;
        cout << "Enter 0 to exit..." << endl;
        cout << "Enter your choice: ";
        cin >> choice;
        switch (choice)
        {
        case 0:
            return 0;
        case 1:
            cout << "Enter element to be searched: ";
            cin >> element;
            cout << element << " is at index: " << linearSearch(array, size, element) << endl;
            break;
        case 2:
            cout << "Enter element to be searched: ";
            cin >> element;
            cout << element << " is at index: " << binarySearch(array, size, element) << endl;
            break;
        }
        cout<<"Please press enter to continue...";
        cin.ignore();
        cin.get();
    }
}

template <typename T>
int binarySearch(T *array, int end, T data, int start)
{
    if (start > end)
        return -1;
    int mid = (start + end) / 2;
    if (array[mid] == data)
        return mid;
    else if (data < array[mid])
        return binarySearch(array, mid, data, start);
    else
        return binarySearch(array, end, data, mid);
}

template <typename T>
int linearSearch(T *array, int size, T data)
{
    for (int i = 0; i < size; i++)
    {
        if (array[i] == data)
            return i;
    }
    return -1;
}

template <typename T>
void display(T *array, int size)
{
    cout<<"{ ";
    for(int i=0;i<size;i++)
        cout<<array[i]<<" ";
    cout<<"}"<<endl;
}